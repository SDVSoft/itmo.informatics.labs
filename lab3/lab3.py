def convert_base(n, to_base=10, from_base=10):
    if isinstance(n, str):
        n = int(n, from_base)
    alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    if n < to_base:
        return alphabet[n]
    return convert_base(n // to_base, to_base) + alphabet[n % to_base]

def fibton(n, to_base=10):
    res = 0; fib_a = 1; fib_b = 1
    for pos in n[::-1]:
        if int(pos):
            res += fib_a
        fib_a += fib_b
        fib_b = fib_a - fib_b
    if to_base != 10: 
        return convert_base(str(res), to_base)
    return str(res)

f = open('input', 'r')
for line in f:
    line = line.split()
    s = ""
    if line[0] == 'Z':
        s += "Z -> " + line[1] + " : "
        base = int(line[1])
        for x in line[2:]:
            if s[-2] != ':':
                s += " | "
            s += x + " -> " + fibton(x, base)
    else:
        s += line[0] + " -> " + line[1] + " : "
        from_base = int(line[0]); to_base = int(line[1])
        for x in line[2:]:
            if s[-2] != ':':
                s += " | "
            s += x + " -> " + convert_base(x, to_base, from_base)
    print(s)
f.close()
