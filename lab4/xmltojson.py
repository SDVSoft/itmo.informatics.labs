def _xml_to_dict(container, f, line=""):
    if not line:
        line = f.readline()
    if line[0] != '<':
        return False
    pos = 1
    tag = ""
    while line[pos] != '>' and line[pos] != ' ' and line[pos] != '/':
        tag += line[pos]
        pos += 1
    cur_cont = tag
    if tag in container:
        if type(container[tag]) is list:
            container[tag] += [{}]
        else:
            container[tag] = [container[tag], {}]
        container = container[tag]
        cur_cont = len(container) - 1
    else:
        container[tag] = {}
    while line[pos] != '>' and line[pos] != '/':
        if line[pos] != ' ':
            attribute = ""
            while line[pos] != '=':
                attribute += line[pos]
                pos += 1
            quote = line[pos + 1]
            pos += 2
            container[cur_cont][attribute] = ""
            while line[pos] != quote:
                container[cur_cont][attribute] += line[pos]
                pos += 1
        pos += 1
    if line[pos] == '/':
        if not container[cur_cont]:
            container[cur_cont] = ""
        return line[pos + 2:]
    pos += 1
    text = ""
    while True:
        while pos < len(line) and (line[pos] == '\t' or line[pos] == '\n'):
            pos += 1
        while pos < len(line) and line[pos] != '<':
            text += line[pos]
            pos += 1
        if pos == len(line):
            line = f.readline()
            pos = 0
        elif line[pos + 1] == '/':
            while line[pos] != '>':
                pos += 1
            if text and text[-1] == '\n':
                text = text[:-1]
            if not container[cur_cont]:
                container[cur_cont] = text
            elif text:
                container[cur_cont]["#" + tag] = text
            return line[pos + 1:]
        else:
            line = _xml_to_dict(container[cur_cont], f, line[pos:])
            pos = 0


def xml_to_dict(src_file):
    d = {}
    _xml_to_dict(d, src_file)
    return d


def dict_to_json(d, res_file, depth=0):
    if type(d) is dict:
        res_file.write('{')
        for i, (key, data) in enumerate(d.items()):
            if '"' in key:
                res_file.write('\r\n' + '\t' * (depth + 1) +
                               "'" + key + "' : ")
            else:
                res_file.write('\r\n' + '\t' * (depth + 1) +
                               '"' + key + '" : ')
            dict_to_json(data, res_file, depth + 1)
            if i + 1 < len(d):
                res_file.write(',')
        res_file.write('\r\n' + '\t' * depth + '}')
    elif type(d) is list:
        res_file.write('[')
        for i, data in enumerate(d):
            res_file.write('\r\n' + '\t' * (depth + 1))
            dict_to_json(data, res_file, depth + 1)
            if i + 1 < len(d):
                res_file.write(',')
        res_file.write('\r\n' + '\t' * depth + ']')
    else:
        if '"' in d:
            res_file.write("'" + str(d) + "'")
        else:
            res_file.write('"' + str(d) + '"')


fin = open("src_xml.txt", "r", encoding="utf-8-sig")
fout = open("res_json.txt", "w")
dict_to_json(xml_to_dict(fin), fout)
